package com.example.animalapi.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.animalapi.models.Cat;
import com.example.animalapi.models.Dog;
import com.example.animalapi.services.CatService;
import com.example.animalapi.services.DogService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class AnimalController {
    @Autowired
    private CatService catService;
    private DogService dogService;

    @GetMapping("/cats")
    public List<Cat> getCats() {
        return catService.createCats();
    }

    @GetMapping("/dogs")
    public List<Dog> getDogs() {
        return dogService.createDogs();
    }
}
